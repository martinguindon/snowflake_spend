{% docs snowflake_warehouse_metering %}

This is the base model for the Snowflake Warehouse Metering History table https://docs.snowflake.net/manuals/sql-reference/account-usage/warehouse_metering_history.html.

{% enddocs %}